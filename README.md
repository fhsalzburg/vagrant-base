﻿# Doku Submission System

## Intro

The Vagrantfile definition downaloads a vagrantbox image from
https://atlas.hashicorp.com. The box with the name *comiq/dockerbox* is
configured for virtualbox and comes with a docker infrastructur. In the
Vagrantfile some additional installations are done. Those are

  * apache2
  * php
  * zip

When started with *vagrant up* a webserver is provided to upload student
submission. When uploaded a docker container is started and a check-script is
running within it, to enshure a secure execution.
The when the container has been running all outputs (STDOUT) are parsed and used
for displaying the result of the homework check nicely formatted.

## HOWTO install & run

Follow the next steps to run the virtual machine. The `Vagrantfile` is
configured für Virtual Box and when started will establish a webserver on
 `localhost:8080`
where the submission system is reachable. For port configuration find & change
the host port from 8080 at line:  
 `config.vm.network "forwarded_port", guest: 80, host: 8080`
to the desired one.

### before start:

  * install vagrant
  * install git
  * install virtualbox

### run VM

`vagrant up`

### ssh into VM

  * Linux:  
  `vagrant ssh`
  * Windows: call  
  `ssh.sh`   
  from command or powershell

### remove VM

To remove the VM

`vagrant destroy`

## Files & Directories

  * ***.zip**: demo files for upload
  * **Vagrantfile**: vagrant definition file
  * **ssh.sh**: start ssh into vm for windows (prerequisite: install git (+bash) for windows)
  * **web/**: folder which contains the submission system (mounted into VM to /var/www/html)
  * **web/readme.txt**: info for submission system.
  * **NOS2017_Eduard_Hirsch_08154711.zip**: demo upload
