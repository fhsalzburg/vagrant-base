<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<meta name="author" content="Andreas Unterweger">
    <title>Ergebnis für Ihre Abgabe</title> <!-- Results for your submission -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <style type="text/css">
      table
      {
        border: 1px solid #CCCCCC;
        border-collapse: collapse;
      }
      td, th
      {
        border: 1px solid #CCCCCC;
        padding-left: 4px;
        padding-right: 4px;
      }
      img
      {
        margin-bottom: -3px; /* Align with text baseline */
      }
      .errtype
      {
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h1>Ergebnis für Ihre Abgabe</h1> <!-- Results for your submission -->
    <?php
      $start = microtime(true);

      function validateLab()
      {
        global $err;
        $first_lab = -1;
        $last_lab = 6;
        if (!isset($_POST['lab']))
        {
          $err = "Keine Labornummer angegeben. <!-- No laboratory number specified. -->";
          return NULL;
        }
        $lab = intval($_POST['lab']);
        if ($lab == 0)
        {
          $err = "Die angegebene Labornummer ($lab) ist keine ganze Zahl. <!-- The selected laboratory number ($lab) is not an integer. -->";
          return NULL;
        }
        if ($lab < $first_lab or $lab > $last_lab)
        {
          $err = "Das angegebene Labor liegt nicht zwischen dem ersten ($first_lab) und dem letzten ($last_lab). <!-- The selected laboratory is not between the first ($first_lab) and the last ($last_lab). -->";
          return NULL;
        }
        return $lab;
      }

      function validateFile()
      {
        global $err;
        $max_size = 10 * 1024 * 1024; /* 10 MiB */
        $file = $_FILES['file'];
        if (!isset($file))
        {
          $err = "Es wurde keine Datei angegeben. <!-- No file has been specified. -->";
          return NULL;
        }
        if ($file['size'] > $max_size)
        {
          $err = "Die hochgeladene Datei ist größer als 10 MiB. <!-- The uploaded file is larger than 10 MiB. -->";
          return NULL;
        }
        if ($file['error'] != 0)
        {
          if ($file['error'] == 1 or $file['error'] == 2)
            $err .= " Die hochgeladene Datei ist viel zu groß. <!-- The uploaded file is far too large. -->"; 
          else
            $err = "Fehler " . $file['error'] . " beim Hochladen der Datei. <!-- Error X uploading file -->";
          return NULL;
        }
        return $file;
      }

      function validateKey()
      {
        global $err;
        $key = $_POST['key'];
        if ($key == NULL)
        {
          $err = "Es wurde kein Schlüssel angegeben. <!-- No key has been specified. -->";
          return NULL;
        }
        return $key;
      }

      function analyzeFile($file)
      {
        global $err;
        $temp_path = $file['tmp_name'];
        $name = basename($file['name']);
        return [$temp_path, $name];
      }

      function getScriptName($lab)
      {
        $scripts = array(
          -1 => "check_V2b.sh",
          1 => "check_AK.sh",
          2 => "check_VK.sh",
          3 => "check_TK.sh",
          4 => "check_MTM.sh",
          5 => "check_OKÜ.sh",
          6 => "check.sh");
        return $scripts[$lab];
      }

      function formatMessages($messages)
      {
        $textonly = false; //Set to true to show only text and no images
        $output = "<table>\n";
        $output .= "      <tr>\n";
        $output .= "        <th>Art</th> <!-- Type -->\n";
        $output .= "        <th>Meldung</th> <!-- Message -->\n";
        $output .= "        <th>Punkte</th> <!-- Points -->\n";
        $output .= "      </tr>\n";
        foreach (explode(PHP_EOL, $messages) as $line)
        {
          if ($line == "")
            continue;
          $output .= "      <tr>\n";
          if (substr($line, 0, 9) != "MESSAGE: ")
          {
            $output .= "        <td class=\"errtype\">\n";
            if ($textonly)
              $output .= "          <em>Unbekannt</em> <!-- Unknown -->\n";
            else
              $output .= "          <img src=\"images/unknown.png\" width=\"16\" height=\"16\" alt=\"Unbekannt\"/> <!-- Unknown -->\n";
            $output .= "        </td>\n";
            $output .= "        <td>\n";
            $output .= "          <pre>" . trim($line) . "</pre>\n";
            $output .= "        </td>\n";           
          }
          else
          {
            $message = substr($line, 9);
            if (substr($message, 0, 3) == "!!!")
            {
              $output .= "        <td class=\"errtype\">\n";
              if ($textonly)
                $output .= "          <em>Fehler</em> <!-- Error -->\n";
              else
                $output .= "          <img src=\"images/error.png\" width=\"16\" height=\"16\" alt=\"Fehler\"/> <!-- Error -->\n";
              $output .= "        </td>\n";
              $message = substr($message, 3);
            }
            else if ($message[0] == "!")
            {
              $output .= "        <td class=\"errtype\">\n";
              if ($textonly)
                $output .= "          <em>Warnung</em> <!-- Warning -->\n";
              else
                $output .= "          <img src=\"images/warning.png\" width=\"16\" height=\"16\" alt=\"Warnung\"/> <!-- Warning -->\n";
              $output .= "        </td>\n";
              $message = substr($message, 1);
            }
            else
            {
              if ($textonly)
                $output .= "        <td class=\"errtype\">Information</td>\n";
              else
              {
                $output .= "        <td class=\"errtype\">\n";
                $output .= "          <img src=\"images/info.png\" width=\"16\" height=\"16\" alt=\"Information\"/>\n";
                $output .= "        </td>\n";
              }
            }
            $message = preg_replace('/\[START CODE\](.*?)\[END CODE\]/', "\n" . '          <br/>' . "\n" . '          <pre>$1</pre>' . "\n" . '          ', $message);
            $message = preg_replace('/\[BREAK]/', "\n", $message);
            $points = NULL;
            if (preg_match('/\[START POINTS\](.*?)\[END POINTS\]/', $message, $matches))
            {
              if (count($matches) >= 2)
                $points = $matches[1];
              $message = preg_replace('/\[START POINTS\](.*?)\[END POINTS\]/', "", $message);
            }
            $output .= "        <td>" . trim($message) . "</td>\n";
            if ($points != NULL)
              $output .= "        <td>$points</td>\n";
            else
              $output .= "        <td></td>\n";
          }
          $output .= "      </tr>\n";
        }
        $output .= "    </table>\n";
        return $output;
      }

      function execScript($script, $file, $filename, $key)
      {
        global $err;
        //$scriptpath = "./docker/";
        $scriptpath = "/home/vagrant/nos-check/";
        $scriptname = "run.sh";
        //echo "<pre>$script</pre>"; echo "<pre>$filename</pre>";
        $command = "bash -c \"(chmod a+r '$file' && cd '$scriptpath' && exec './$scriptname' '$script' '$file' '$filename' '$key')\" 2>&1";

        //echo "<pre>$command</pre>";
        //mkdir($scriptpath . "/homework");
        //$command = "bash -c \"(cd '$scriptpath' && exec './$scriptname')\" 2>&1";

        $handle = popen($command, "r");
        $ret = "";
        while (!feof($handle))
        {
          $buffer = fgets($handle, 1024);
          $ret .= "$buffer";
        }
        $retcode = pclose($handle);
        if ($retcode != 0)
        {
          if ($retcode == 2) //Timeout
            $err = trim($ret); //Print message
          else
            $err = "Error $retcode executing script.";
          return NULL;
        }
        #echo "<pre>$ret</pre>";
        return $ret;
      }

      global $err;
      if (!isset($_POST) or empty($_POST))
      {
        echo "<strong>Parameterfehler:</strong> Es scheinen keine Parameter angegeben zu sein. Das passiert, wenn die Größe der hochgeladenen Datei die maximal erlaubte Dateigröße weit überschreitet. <!-- <strong>Parameter error:</strong> There seem to be no parameters. This happens when the size of the uploaded file exceeds the maximum allowed file size by far. -->";
      }
      else
      {
        $lab = validateLab();
        if ($lab != NULL)
        {
          //validate size only
          $file = validateFile();
          if ($file != NULL)
          {
            // key check
            $key = validateKey();
              //$key = "dummy";
          }
        }
        if ($lab == NULL or $file == NULL or $key == NULL)
        {
            if ($lab == NULL) echo "lab<br>";
            if ($file == NULL) echo "file<br>";
            if ($key == NULL) echo "key<br>";

          echo "<pre>" . print_r($file, true) . "</pre><pre>" . print_r($lab). "</pre><pre>" . print_r($key). "</pre> <!-- Parameter error: --> $err";
        }
        else
        {
          list($file, $filename) = analyzeFile($file);
          $script = getScriptName($lab);
          $result = execScript($script, $file, $filename, $key);
          if ($result == NULL)
          {
            echo "<strong>Ausführungsfehler:</strong> <!-- Execution error: --> $err";
          }
          else
          {
            echo formatMessages($result);
            echo "    <p>Beachten Sie, dass dies nur zu Ihrer Information ist. Verwenden Sie das <a href=\"https://elearn.fh-salzburg.ac.at/lms/\" target=\"_blank\">E-Learning-System der Fachhochschule Salzburg</a>, um Abgaben zur Beurteilung hochzuladen.</p> <!-- Note that this is for your information only. Use the <a href=\"https://elearn.fh-salzburg.ac.at/lms/\" target=\"_blank\">E-Learning system of the Salzburg University of Applied Sciences</a> to upload submissions for grading. -->";
          }
        }
      }
      $end = microtime(true);
      $diff = $end - $start;
      echo "\n    <p>\n";
      echo "      <small>Die Verarbeitung dauerte zirka " . number_format($diff, 1) . " Sekunden.</small> <!-- Processing took around X seconds. -->\n";
      echo "    </p>\n";
    ?>
	<p>
	  <small>Diese Seite basiert auf einer Implementierung von Andreas Unterweger.</small> <!-- This page is based on an implementation by Andreas Unterweger. -->
	</p>
  </body>
</html>